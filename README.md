# USTB 机器学习专选课作业


. 代码见 LDA.py
. 数据集 watermelon.csv

### 计算结果
计算值
> [1 1 1 1 1 0 0 0 1 0 0 0 1 1 0 0]

真值
> [1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0]

加入奇异值分解，在本次计算中是否采用奇异值分解，结果相同
```python
 Sw = np.matmul((self.x1-self.meanx1).T,(self.x1-self.meanx1)) + np.matmul((self.x2-self.meanx2).T,(self.x2-self.meanx2))
 u, s, vh = np.linalg.svd(Sw) # sw = u diag(S) vT
 self.w =np.matmul( np.matmul( np.transpose(vh) , np.linalg.inv(np.diag(s)) ) , np.transpose(u))
 self.w = np.dot(self.w, (self.meanx1 - self.meanx2).T).reshape(1, -1) 

```

![output](output.png)


> 绘图部分借鉴代码 https://github.com/han1057578619/MachineLearning_Zhouzhihua_ProblemSets/blob/master/ch3--%E7%BA%BF%E6%80%A7%E6%A8%A1%E5%9E%8B/3.5/3.5-LDA.py

